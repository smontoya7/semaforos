'''
    Solución para el proyecto Semáforos
'''

# módulo para la lectura de los datos del proyecto
import lectura
# módulo para modelar la ciudad en el proyecto
from estructuras import *


def CrearIntersecciones(inters: list):
    """
    Crea las intersecciones y añade en cada una sus calles correspondientes

    Args:
        inters (list): lista (vacía) de las V intersecciones
    """
    print("Creando intersecciones ...", end="\t")

    # se añaden las V intersecciones vacías
    for i in range(V):
        inters.append(Interseccion())

    # se revisan todas las calles y se añaden a las intersecciones
    for c in calles:
        inters[c.desde].addSaliente(c)
        inters[c.hacia].addEntrante(c)

    print("OK")


def ProgramarSemaforos(inters: list)->list:
    """
    Calcula las programaciones de todos los semáforos

    Args:
        inters (list): lista de intersecciones

    Returns:
        list: lista de listas que describen la programación de cada semáforo
    """
    print("Programando semáforos ...", end="\t")
    res = []

    # por cada intersección
    for i in range(V):
        # objeto correspondiente a la intersección i
        ins = inters[i]

        # calcula la programación para la intersección i
        prog = ins.Programacion()

        # la programacion no cuenta ningún semáforo
        if (len(prog) == 0):
            continue
        else:
            res.append((i, prog))

    print("OK")
    return res


def ContarFlujos(calles: list, trayectos: list):
    """
    Cuenta los flujos en las distintas calles pasando por cada trayecto

    Args:
        calles (list): lista de calles
        trayectos (list): lista de trayectos
    """
    print("Contando flujos ...", end="\t")

    # por cada trayecto
    for tray in trayectos:
        # por cada nombre de calle en el trayecto
        for cnombre in tray:
            # índice correspondiente al objeto de la calle
            idx = codigos_calles[cnombre]

            calles[idx].flujo += 1

    print("OK")


'''
    Ejecución
'''
V, E, C = 0, 0, 0
calles = []
inters = []
codigos_calles = {}
programa = []
trayectos = []

# lee del archivo de red.txt
V, E = lectura.LeerRed(calles, inters, codigos_calles)
# lee del archivo de trayectos.txt
C = lectura.LeerTrayectos(trayectos)
# lee del archivo de simulacion.txt


# crea la representación del grafo
CrearIntersecciones(inters)

# cuenta el flujo de carros para cada calle
ContarFlujos(calles, trayectos)

# crea la programación de los semáforos para cada semáforo
res = ProgramarSemaforos(inters)

# lo imprime al archivo programacion.txt
lectura.ImprimirSolucion(res)