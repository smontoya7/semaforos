PROYECTO DE SEMAFOROS 

PASOS PARA EJECUTAR EL PROGRAMA

CLONAR EL PROYECTO 

PYCHARM

1. Instalar
- https://www.jetbrains.com/es-es/pycharm/download/#section=windows
- 	Descargar Python de https://www.python.org/
- 	Seleccionar el intérprete (preferiblemente el intérprete de Python.org; los otros pueden presentar fallos). Buscar en “Archivos de programa (x86)” si VSCode no detecta automáticamente.


2. Agregra un venv en el editor pycharm y añadir un interprete de PYTHON VERSION 3.9 O SUPERIOR 
3. Ejecutar el archivo main.py 
     Este genera un archivo llamado programacion.txt
4. Ejecutar el archivo simulacion.py (este archivo leera los resultado del
 achivo final programacion para calcular el puntaje y efectividad de la semaforizacion)

 
Resultado arrojado por consola

- Leyendo red.txt ...	OK
- Leyendo trayectos.txt ...	OK
- Leyendo simulacion.txt ...	OK
- Leyendo programacion.txt ...	OK
- Calculando puntaje máximo ...		OK
- Configurando filas ...		OK
- Simulando programación ...		OK
- Efectividad: 97.87 % ( 5001697 / 5110441 )


Process finished with exit code 0


VSCode

1.	Instalar VSCode desde el siguiente link: https://code.visualstudio.com/
2.	Añadir las extensiones requeridas a VSCode (C/C++, IntelliSense, IntelliCode son las recomendadas)
 •	Si se selecciona VSCode para interpretar, se puede usar la extensión de Python + Python de la Windows Store como interprete o la versión de Python descargada. Para seleccionar cuál: Ctrl + Shift + P, "Python: Seleccionar Intérprete"
 
 
********************   PLUS QUE DESARROLLAMOS **********************************

Creamos un archivo en c++ para generar aleatoriamente los archivos red.txt - simulador.txt - trayectos.txt 
 
Para actualizar los rangos que generaran los archivos aleatoriamente debera modificar el archivo generador.cpp :

rangos para archivo trayectos.txt

```
void ConstruirTrayectos() {
    ofstream trayfile;
    const int CMIN = 5;
    const int CMAX = 20; 
```

rangos para archivo red.txt

```
void ConstruirRed() {
    ofstream redfile;
    const int VMIN = 5; // Minimo vertices
    const int VMAX = 8;  // Maximo vertices
    const int EMIN = 10; // Minimo de aristas 
    const int EMAX = 20; // Maximo de aristas
    // corresponde a los pesos de las aristas
    const int WMIN = 5;
    const int WMAX = 100;
	
```

rangos para archivo trayectos.txt

```
void ConstruirSimulacion() {
    ofstream simfile;
    const int TMIN = 10;  // Trayectos minimos
    const int TMAX = 1000; // Trayectos maximos
    const int PMIN = 10; // Puntaje minimo
    const int PMAX = 300; // Puntaje maximo
```

	
Requisitos y pasos para compilar el programa en c++

C++
1.	Instalar MinGW de https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download 
2.	Copiar la dirección de la carpeta bin de MinGW. Suele estar yendo a C:\Archivos de programa (x86)\mingw-w64 …
3.	En Windows, ir a Configuración > Editar las variables de esta cuenta > Variables de entorno > Seleccionar PATH. Añadir la dirección del bin de MinGW
4.	Para revisar la instalación, abrir CMD y escribir "g++ --version" y "gdb --version"
5.	Reiniciar el computador
6.	Reiniciar VSCode (Ctrl + Shift + P, "Reload Window")
 
•	Al usar C++ en VSCode, se recomienda crear una carpeta donde se guardarán todos los programas y abrir esa carpeta.
 
•	En la consola de VSCode usar powershell
 
Comando de compilación 
g++ -o nombre_del_ejecutable prueba.cpp
 
Comando de ejecución (ejecutable.exe) : 
./ejecutable.exe
o
start ejecutable
