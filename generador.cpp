#include <bits/stdc++.h>
using namespace std;

// abreviaciones
typedef long long ll;
typedef long double ld;
#define pb push_back
#define ff first
#define ss second
#define endl "\n"

// optimización para lectura de grandes formatos
void fast_io() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
}

const ll NMAX = 1e5+5;

int v, e;
// aristas que ya se vieron. Funciona como un 'visited'
set <pair <int,int> > aristas_vistas;
// nombre de una calle --> (x,y) tal que x->y
unordered_map <string, pair <int,int> > nombres_nodos;
// (x,y) tal que x->y --> nombre de la calle
map <pair <int,int>, string> nodos_nombres;
// nombre de una calle --> peso (tiempo para recorrer) de la calle
unordered_map <string, int> pesos;
// lista de adyacencia para el grafo
vector <int> Adj[NMAX];


// genera entre [a,b]
int rand(int a, int b) {
    return a + rand() % (b-a+1);
}

// crea el nombre para la calle i-ésima
string nombre_calle(int x) {
    return "calle" + to_string(x);
}

// genera una semilla aleatoria
void AleatorizarSemilla() {
    cout << "Aleatorizando semilla ... ";
    srand(time(0));
    cout << "OK" << endl;
}

void ConstruirRed() {
    ofstream redfile;
    const int VMIN = 5;
    const int VMAX = 8;
    const int EMIN = 10;
    const int EMAX = 20;
    // corresponde a los pesos de las aristas
    const int WMIN = 5;
    const int WMAX = 100;

    cout << "Construyendo red.txt ... ";
    redfile.open("red.txt");

    // definimos v y e de manera aleatoria
    v = rand(VMIN, VMAX);
    e = rand(EMIN, min(EMAX, v*(v-1)/2));

    // creamos las aristas
    for (int i=0; i<e; i++) {
        int x, y, w;
        string nombre;

        w = rand(WMIN, WMAX);
        do {
            x = rand(0, v-1);
            y = rand(0, v-1);
        } while (x == y || aristas_vistas.count({x,y}));

        // la contamos como vista
        aristas_vistas.insert({x,y});
        // creamos un nombre
        nombre = nombre_calle(i);
        nombres_nodos[nombre] = {x,y};
        nodos_nombres[{x,y}] = nombre;
        pesos[nombre] = w;
    }

    // imprimimos los valores
    redfile << v << " " << e << endl;
    for (int i=0; i<e; i++) {
        string nombre = nombre_calle(i);
        pair <int,int> intersecs = nombres_nodos[nombre];
        int peso = pesos[nombre];

        redfile << intersecs.ff << " " << intersecs.ss << " " << nombre << " " << peso << endl;
    }

    redfile.close();
    cout << "OK" << endl;
}

void ConstruirTrayectos() {
    ofstream trayfile;
    const int CMIN = 5;
    const int CMAX = 20;
    const int CONTADOR_MAX = 5;

    int c;

    cout << "Construyendo trayectos.txt ... ";

    trayfile.open("trayectos.txt");

    // construimos el grafo
    for (pair <int,int> p : aristas_vistas) {
        Adj[p.ff].pb(p.ss);
    }

    // número de trayectos aleatorio
    c = rand(CMIN, CMAX);

    vector <vector <string> > trayectos;
    vector <string> cadena_calles;
    set <string> seen;

    // generamos los trayectos
    for (int i=0; i<c; i++) {
        int curr = rand(0, v-1);
        int idx, contador = 0;
        string nombre;
        pair <int,int> rep_calle;

        while (contador<CONTADOR_MAX) {
            // no hay calles por seleccionar
            if (Adj[curr].empty()) break;

            // selecciona una calle
            idx = rand(0, Adj[curr].size()-1);
            // representación en pair de la calle
            rep_calle = {curr, Adj[curr][idx]};
            // nombre de la calle
            nombre = nodos_nombres[rep_calle];

            // ya la vimos 
            if (seen.count(nombre)) contador++;
            // sirve
            else {
                contador = 0;
                cadena_calles.pb(nombre);
                seen.insert(nombre);
                curr = Adj[curr][idx];
            }
        }

        // Quedó un camino vacío
        if (cadena_calles.empty()) i--;
        // guardamos el trayecto
        else trayectos.pb(cadena_calles);

        cadena_calles.clear();
        seen.clear();
    }

    // imprimimos los trayectos
    trayfile << c << endl;

    for (vector <string> tray : trayectos) {
        trayfile << tray.size() << " ";
        for (string s : tray) {
            trayfile << s << " ";
        }
        trayfile << endl;
    }

    trayfile.close();
    cout << "OK" << endl;
}

void ConstruirSimulacion() {
    ofstream simfile;
    const int TMIN = 10;
    const int TMAX = 1000;
    const int PMIN = 10;
    const int PMAX = 300;
    int p, t;

    cout << "Construyedo simulacion.txt ... ";
    simfile.open("simulacion.txt");

    // definimos p y t de manera aleatoria
    t = rand(TMIN, TMAX);
    p = rand(PMIN, PMAX);

    // imprimimos los valores
    simfile << t << endl;
    simfile << p << endl;

    simfile.close();
    cout << "OK" << endl;
}


int main() {
    AleatorizarSemilla();
    ConstruirRed();
    ConstruirTrayectos();
    ConstruirSimulacion();
    return 0;
}