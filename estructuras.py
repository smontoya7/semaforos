'''
    Clases utilizadas para resolver el proyecto
'''

class Calle:
    def __init__(self, desde: int, hacia: int, nombre: str, peso: int) :
        """
        Constructor de la clase

        Args:
            desde (int): # del nodo de partida
            hacia (int): # del nodo de llegada
            nombre (str): nombre de la calle
            peso (int): tiempo para recorrer la calle
        """
        self.desde = desde
        self.hacia = hacia
        self.nombre = nombre
        self.peso = peso
        # esta variable se utilizará para determinar calles vacías
        self.flujo = 0


class Interseccion:
    def __init__(self):
        """
        Constructor de la clase
        """
        # aristas que entran a la intersección (tendrán semáforos)
        self.entrantes = []
        # aristas que salen de la intersección (no tendrán semáforos)
        self.salientes = []


    def addEntrante(self, cal: Calle):
        """
        Añade una calle como entrante (entra a la intersección)

        Args:
            cal (Calle): la calle entrante
        """
        self.entrantes.append(cal)

    def addSaliente(self, cal: Calle):
        """
        Añade una calle como saliente (sale de la intersección)

        Args:
            cal (Calle): la calle saliente
        """
        self.salientes.append(cal)


    def Programacion(self)->list:
        """
        Encuentra la programación de la programación

        Returns:
            list: lista de tuplas con el nombre de la calle y su duración
        """
        res = []
        
        # se revisan las calles entrantes a la intersección
        for c in self.entrantes:
            # solo se añaden las calles si tienen flujo de carros
            if c.flujo > 0:
                res.append((c.nombre, 1))

        return res


class Carro:
    def __init__(self, id: int):
        self.id = id
        self.paso = 0
        self.t_curr = 0
        self.curr_calle = None


    def nextCalle(self, trayectos: list)->bool:
        """
        Actualiza el paso y la calle a la que va a ir después

        Args:
            trayectos (list): lista de los trayectos
        Returns:
            bool : verdadero si ya acabó el trayecto
        """
        # se acabó el recorrido
        if (self.paso+1 >= len(trayectos[self.id])):
            self.curr_calle = None
            return True
        else:
            self.paso += 1
            self.curr_calle = trayectos[self.id][self.paso]
            return False

    
    def addTiempo(self, t: int):
        """
        Añade el tiempo t al tiempo transcurrido del carro

        Args:
            t (int): tiempo que se quiere añadir
        """
        self.t_curr += t


    def setCalle(self, cal: str):
        """
        Define la calle actual

        Args:
            cal (str): nombre de la calle
        """
        self.curr_calle = cal


    # Comparador <
    def __lt__(self, B):
        if (self.t_curr < B.t_curr): return True
        elif (self.t_curr > B.t_curr): return False
        else: return self.id < B.id
