'''
    Programa encargado de simular el puntaje máximo posible, 
    ejecutar la programación en programacion.txt,
    y comparar los resultados
'''

# módulo para la lectura de los datos del proyecto
from estructuras import Carro
import lectura
from estructuras import *
from queue import PriorityQueue


def PuntajeMax(calles: list, codigos_calles: dict, trayectos: list, t: int, p: int)->int:
    """
    Para cada trayecto, calcula el puntaje obtenido si se suponen todos los semáforos en verde, todo el tiempo. Este puntaje (Pmax) es un techo para cualquier puntaje obtenible (Pmax >= P)

    Args:
        calles (list): lista de las calles
        codigos_calles (dict): nombre de la calle --> índice de la calle
        trayectos (list): lista de los trayectos
        t (int): tiempo de la simulación
        p (int): puntaje otorgado a cada carro que pasa

    Returns:
        int: Puntaje máximo (Pmax)
    """
    print("Calculando puntaje máximo ...", end="\t\t")
    res = 0

    for tray in trayectos:
        t_curr = 0

        # simulamos el recorrido tray
        for cnombre in tray:
            idx = codigos_calles[cnombre]
            cal = calles[idx]

            # TODO: se supone que el tiempo para cruzar la intersección son 0s. ¿Podrían ser 1s?
            t_curr += cal.peso

        # completó el viaje
        if (t_curr <= t):
            res += p
            res += t-t_curr
            
    
    print("OK")
    return res


def ConfigFilas(E: int, filas: list, trayectos: list, calles: list, codigos_calles: dict, curr_inter: list):
    """
    Configura las filas para el primer momento

    Args:
        E (int): número de calles
        filas (list): lista de calles
        trayectos (list): lista de listas de trayectos
        calles (list): lista de representaciones de calles
        codigos_calles (list): relaciona nombre de la calle con su índice en calles
    """
    print("Configurando filas ...", end="\t\t")

    # llenamos las filas con PQs vacías
    for i in range(E):
        filas.append(PriorityQueue())

    # llenamos las filas con los primeros carros
    for i in range(C):
        # objeto del carro
        carro = Carro(i)

        # información del trayecto
        tray = trayectos[i]
        cal = tray[0]
        cal_idx = codigos_calles[cal]
        cal_rep = calles[cal_idx]

        carro.setCalle(cal) # Suponemos que tray por lo menos tiene un elemento
        carro.addTiempo(cal_rep.peso)
        filas[cal_idx].put(carro)


    # llenamos los semáforos actuales en blanco
    for i in range(V):
        curr_inter.append(None)

    print("OK")


def SimProgramacion(T: int, P: int, prog: list, curr_inter: list, trayectos: list, codigos_calles: dict, filas: list)->int:
    """
    Simula la programación propuesta

    Args:
        T (int): tiempo de la simulación
        P (int): puntaje por carro que complete trayecto
        prog (list): lista de listas de programación de cada intersección
        curr_inter (list): lista de actual calle en verde
        trayectos (list): lista de listas de nombres de calles
        codigos_calles (dict): relaciona nombres con índices de calles
        filas (list): lista de queues para cada calle

    Returns:
        (int) el puntaje de la simulación
    """
    print("Simulando programación ...", end="\t\t")
    res = 0

    # para cada segundo
    for t in range(T+1):
        # para cada intersección
        for i in range(V):
            # programación de la intersección
            pr = prog[i]
            dur = dur_ciclo[i]

            # no existe programación para esta intersección
            if (len(pr) == 0):
                continue

            # actualizamos a la calle que esté en verde
            if (t%dur in pr):
                curr_inter[i] = pr[t%dur]

            # calle en verde
            curr_green = curr_inter[i] # TODO: no se está eligiendo correctamente la calle en verde
            cal_idx = codigos_calles[curr_green]

            # hay algún carro
            if (not filas[cal_idx].empty()):
                carro = filas[cal_idx].get()
                # no llegó al frente. Se vuelve a insertar
                if (carro.t_curr > t):
                    filas[cal_idx].put(carro)

                # ya llegó al frente de la fila
                else:
                    # tiempo de espera en el semáforo
                    carro.addTiempo(t - carro.t_curr)

                    carro.nextCalle(trayectos)

                    # va a una calle válida
                    if (carro.curr_calle != None):
                        next_calle_idx = codigos_calles[carro.curr_calle]
                        cal = calles[next_calle_idx]
                        carro.addTiempo(cal.peso) # TODO: no se está añadiendo correctamente el tiempo del semáforo
                        filas[next_calle_idx].put(carro)
                    # acabó su recorrido
                    else:
                        # sumamos su puntaje
                        res += P + (T - carro.t_curr)

    print("OK")
    return res




'''
    Ejecución
'''
V, E, C, T, P = 0, 0, 0, 0, 0
puntaje_max = 0
calles = []
inters = []
codigos_calles = {}
trayectos = []

# variables para la simulacion

# calle en verde actualmente
curr_inter = []
# programación de cada interseccion. Compuesto de listas de sumas de prefijos
prog = []
# duración de un ciclo de programacion
dur_ciclo = []
# queues de las calles
filas = []


# lee del archivo de red.txt
V, E = lectura.LeerRed(calles, inters, codigos_calles)
# lee del archivo de trayectos.txt
C = lectura.LeerTrayectos(trayectos)
# lee el archivo de la simulación
T, P = lectura.LeerSimulacion()
# lee el archivo de la programación
lectura.LeerProgramacion(V, prog, dur_ciclo)

# calcula el puntaje máximo
puntaje_max = PuntajeMax(calles, codigos_calles, trayectos, T, P)

# configuramos las filas de las calles iniciales
ConfigFilas(E, filas, trayectos, calles, codigos_calles, curr_inter)

# calcula el puntaje con la programación
puntaje = SimProgramacion(T, P, prog, curr_inter, trayectos, codigos_calles, filas)

if (puntaje_max == 0):
    print("Efectividad: 100% (0 / 0)")
else:
    print("Efectividad:", round(puntaje/puntaje_max*100, 2), "% (", puntaje, "/", puntaje_max, ")")