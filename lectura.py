'''
    Funciones de lectura y escritura
'''

# módulo para modelar la ciudad en el proyecto
from os import listdir
from estructuras import *

# esta constante se puede cambiar de acuerdo al formato de los archivos a leer. En los casos de archivos de prueba con nombres como red10.txt, el sufijo sería "10"
SUFIJO = ""


def LeerRed(calles: list, inters: list, codigos_calles: dict):
    """
    Lee el archivo red.txt

    Args:
        calles (list): lista de calles
        inters (list): lista de intersecciones
        codigos_calles (dict): mapa de las calles con sus códigos
    Returns:
        V, E (tuple): número de intersecciones y número de calles
    """
    nom_archivo = "red" + SUFIJO + ".txt"
    print("Leyendo", nom_archivo, "...", end="\t")
    red = open(nom_archivo, "r")

    # Leemos la primera linea
    entrada = red.readline().strip().split()
    V, E = [int(x) for x in entrada]

    # Leemos las aristas
    for i in range(E):
        entrada = red.readline().strip().split()
        u = int(entrada[0])
        v = int(entrada[1])
        nom = str(entrada[2])
        w = int(entrada[3])

        calles.append(Calle(u, v, nom, w))
        codigos_calles[nom] = i

    red.close()
    print("OK")
    return V, E


def LeerTrayectos(trayectos: list):
    """
    Lee el archivo trayectos.txt

    Args:
        trayectos (list): lista de los trayectos
    Returns:
        int : número de trayectos   
    """
    nom_archivo = "trayectos" + SUFIJO + ".txt"
    print("Leyendo", nom_archivo, "...", end="\t")
    tray = open(nom_archivo, "r")

    # Leemos la primera linea
    entrada = tray.readline().strip().split()
    C = int(entrada[0])

    # Leemos las aristas
    for i in range(C):
        entrada = tray.readline().strip().split()
        vias = entrada[1:]
        trayectos.append(vias)

    tray.close()
    print("OK")
    return C


def LeerSimulacion():
    """
    Lee el archivo simulacion.txt

    Returns: 
        T, P
    """ 
    t = 0
    p = 0

    nom_archivo = "simulacion" + SUFIJO + ".txt"
    print("Leyendo", nom_archivo, "...", end="\t")
    simfile = open(nom_archivo, "r")

    entrada = simfile.readline().strip().split()
    t = int(entrada[0])

    entrada = simfile.readline().strip().split()
    p = int(entrada[0])

    print("OK")
    return t, p


def LeerProgramacion(V: int, prog: list, dur_ciclo: list):
    """
    Lee la programación de los semáforos y lo guarda en una lista de prefijos

    Args:
        V (int) : número de intersecciones
        prog (list): lista de listas de suma de prefijos para los tiempos de cada semáforo
        dur_ciclo (list) : lista de int de duraciones de los ciclos
    """
    nom_archivo = "programacion" + SUFIJO + ".txt"
    print("Leyendo", nom_archivo, "...", end="\t")
    progfile = open(nom_archivo, "r")

    entrada = progfile.readline().strip().split()

    num = int(entrada[0])
    # interseccion actual
    curr = 0
    # interseccion leída
    inter_read = 0

    # iteramos por todas las intersecciones
    for i in range(num):
        entrada = progfile.readline().strip().split()
        inter_read = int(entrada[0])

        # tenemos que llegar hasta esa intersección
        while (curr < inter_read):
            prog.append({})
            dur_ciclo.append(-1)
            curr += 1

        # leemos la programación
        entrada = progfile.readline().strip().split()
        num_calles = int(entrada[0])
        # tiempo acumulado
        tacc = 0
        # programacion
        pr = {}

        for j in range(num_calles):
            entrada = progfile.readline().strip().split()
            nombre, dur = entrada[0], int(entrada[1])
            # añadimos el semáforo
            pr[tacc] = nombre
            tacc += dur

        # duración del ciclo
        dur_ciclo.append(tacc)

        prog.append(pr)
        curr += 1

    # terminamos de llenar las duraciones para las intersecciones restantes
    while (len(prog) < V):
        prog.append({})
        dur_ciclo.append(-1)

    print("OK")


def ImprimirSolucion(res: list)->None:
    """
    Imprime la solución al archivo de programación.txt

    Args:
        res (list): lista con las programaciones de todos los semáforos
    """
    nom_archivo = "programacion" + SUFIJO + ".txt"
    print("Escribiendo a", nom_archivo, "...", end="\t")
    salida = open(nom_archivo, "w")


    # len(res) corresponde al numero de intersecciones que se quieren intervenir
    salida.write(str(len(res)) + "\n")

    for elem in res:
        # identificador de la intersección
        id = elem[0]
        # programación de la intersección
        prog = elem[1]
        # número semáforos a intervenir en la intersección
        num = len(prog)

        salida.write(str(id) + "\n")
        salida.write(str(num) + "\n")
        for cpair in prog:
            salida.write(str(cpair[0]) + " " + str(cpair[1]) + "\n")
    
    salida.close()
    print("OK")